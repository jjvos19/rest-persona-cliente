package bo.com.fie.cognos.rest.personas.cliente.entities;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.ext.Provider;

// Se implementa un filter desde el lado del cliente, para la autenticacion.
// Luego se debe llamar en el cliente con register.
// Tarea: implementar la interface ClientResponseFilter para evitar que llegue el objeto RespuestaServicio.
@Provider
public class ClientePersonaFilter implements ClientRequestFilter {
	public void filter(ClientRequestContext requestContext) throws IOException {
		// TODO Auto-generated method stub
		String credenciales = "jjvos:contrasenha";
		byte[] credencialesEncode = Base64.getEncoder().encode(credenciales.getBytes(StandardCharsets.UTF_8));
		requestContext.getHeaders().add("Authorization",
				"Basic " + new String(credencialesEncode, StandardCharsets.UTF_8));
		System.out.println(credenciales);
		System.out.println(new String(credencialesEncode));
		System.out.println(new String(credencialesEncode, StandardCharsets.UTF_8));
	}

}
