package bo.com.fie.cognos.rest.personas.cliente;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import bo.com.fie.cognos.rest.personas.cliente.entities.ClientePersonaFilter;
import bo.com.fie.cognos.rest.personas.cliente.entities.IPersonaCliente;
import bo.com.fie.cognos.rest.personas.cliente.entities.Persona;

public class ClienteRestEasy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		ResteasyClient client = (ResteasyClient) ResteasyClientBuilder.newBuilder().build();
		ResteasyClient client = new ResteasyClientBuilder().build();

		// Se llama al filter para enviar la autenticacion.
		client.register(ClientePersonaFilter.class);

		// Se toma hasta el rest, porque el resto se defin en el cliente persona.
		ResteasyWebTarget target = client.target(UriBuilder.fromPath("http://localhost:8080/rest-personas/rest/"));

		IPersonaCliente personaCliente = target.proxy(IPersonaCliente.class);
		Response response = null;

		response = personaCliente.getPersona(2L);
		Persona persona = response.readEntity(Persona.class);
		System.out.println("Persona: " + persona);
		persona.setPaterno("Picapiedra");
		personaCliente.actualizarPersona(persona);
		response = personaCliente.getPersona(2L);
		persona = response.readEntity(Persona.class);
		System.out.println("Persona Act.: " + persona);
		Persona nueva = new Persona();
		nueva.setEdad(15);
		nueva.setNombre("Mini");
		nueva.setPaterno("Mouse");
		nueva.setMaterno("Disney");
		personaCliente.insertarPersona(nueva);
		personaCliente.insertarPersona("Gargamel", "Brujo", "Negro", 55);
		response = personaCliente.listarPersonas();
		// El el caso de que no se envie una lista o persona, se 
		// envia el objeto RespuetaServicio, en tal caso existira un problema al parsear.
		// Aqui se debe utilizar el ClientResponseFilter y evitar que de un error.
		if (response.getStatus() == 200) {
			List<Persona> lista = response.readEntity(new GenericType<List<Persona>>() {
			});
			// Lista todas las personas.
			for (Persona p : lista) {
				System.out.println(p);
			}
		} else {
			System.out.println("Error al invocar el servicio");
		}
	}

}
