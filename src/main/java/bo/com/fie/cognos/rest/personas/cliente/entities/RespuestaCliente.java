package bo.com.fie.cognos.rest.personas.cliente.entities;

public class RespuestaCliente {

	private String codigo;
	private String mensaje;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public RespuestaCliente() {
		this.codigo = "?";
		this.mensaje = "?";
	}
	
	public RespuestaCliente(String codigo, String mensaje) {
		this();
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
}
