package bo.com.fie.cognos.rest.personas.cliente.entities;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("personas")
public interface IPersonaCliente {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	Response listarPersonas();

	// @PathParam Se utiliza cuando se pasa los parametros por la URL.
	@GET
	@Path("/buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getPersona(@PathParam("id") long id);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	void insertarPersona(Persona persona);

	// En la url ha que adicionar
	// http://localhost:8080/rest-personas/rest/personas/form
	// POST y en body se le coloca x-www-form-urlencode
	// @FormParam Se utiliza para indicar que pasa por formulario los datos.
	@POST
	@Path("/form")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	void insertarPersona(@FormParam("nombre") String nombre, @FormParam("paterno") String paterno,
			@FormParam("materno") String materno, @FormParam("edad") int edad);

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	void actualizarPersona(Persona persona);

	@DELETE
	@Path("/{id}")
	void borrarPersona(@PathParam("id") long id);

}
